@extends('layouts.app')
 
@section('title', 'Interviews')

@section('content')

<div><a href =  "{{url('/interviews/create')}}"> Add new interview</a></div>

<h1>List of interviews</h1>
<table class = "table table-dark">
    <tr>
        <th>id</th><th>Interview date</th><th>Candidate</th><th>User</th><th>Summary</th><th>Created</th><th>Updated</th>
    </tr>
    <!-- the table data -->
    @foreach($interviews as $interview)
    <tr>
        <td>{{$interview->id}}</td>
        <td>{{$interview->date_interview}}</td>
        <td>
            @if(isset($interview->candidate_id))
            {{$interview->candidate->name}}  
            @else
                Assign candidate
            @endif
            </td>
            <td>
                @if(isset($interview->user_id))
                {{$interview->user->name}}  
                @else
                    Assign user
                @endif
                </td>
            <td>{{$interview->summary}}</td>                           
            <td>{{$interview->created_at}}</td>
            <td>{{$interview->updated_at}}</td>
                                                                                       
    </tr>
    @endforeach
</table>
@endsection